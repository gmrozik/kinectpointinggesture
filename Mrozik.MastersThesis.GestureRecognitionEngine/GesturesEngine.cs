﻿using System;
using System.Collections.Generic;
using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition;

namespace Mrozik.MastersThesis.GestureRecognitionEngine
{
    public class GesturesEngine : IGesturesEngine
    {
        private readonly IKinectSensorWrapper _sensorWrapper;
        private readonly Func<Skeleton[], Skeleton> _skeletonSelector;
        private readonly IList<IGesture> _gestures = new List<IGesture>();

        public GesturesEngine(IKinectSensorWrapper sensorWrapper, Func<Skeleton[], Skeleton> skeletonSelector)
        {
            _sensorWrapper = sensorWrapper;
            _skeletonSelector = skeletonSelector;
        }

        public void AddGestureDefinition(IGesture gesture)
        {
            _gestures.Add(gesture);
            gesture.GestureRecognized += OnGestureRecognized;
        }

        public void Start()
        {
            _sensorWrapper.KinectSensor.AllFramesReady += KinectSensor_AllFramesReady;

        }

        public void Stop()
        {
            if (_sensorWrapper != null)
            {
                _sensorWrapper.KinectSensor.AllFramesReady -= KinectSensor_AllFramesReady;
            }
        }


        void KinectSensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            using (var colorImageFrame = e.OpenColorImageFrame())
            {
                if (colorImageFrame == null)
                    return;

                using (var depthImageFrame = e.OpenDepthImageFrame())
                {
                    if (depthImageFrame == null)
                        return;

                    using (var skeletonFrame = e.OpenSkeletonFrame())
                    {
                        if (skeletonFrame == null)
                            return;

                        var skeletons = new Skeleton[6];
                        skeletonFrame.CopySkeletonDataTo(skeletons);
                        var skeleton = _skeletonSelector(skeletons);
                        if (skeleton != null)
                        {
                            foreach (var gesture in _gestures)
                                gesture.UpdateSkeletonData(colorImageFrame, depthImageFrame, skeletonFrame, skeleton);
                        }

                    }
                }
            }
        }

        public event EventHandler<RecognizedGestureEventArgs> GestureRecognized;
        protected virtual void OnGestureRecognized(object sender, RecognizedGestureEventArgs e)
        {
            EventHandler<RecognizedGestureEventArgs> handler = GestureRecognized;
            if (handler != null) handler(sender, e);
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
