﻿namespace Mrozik.MastersThesis.GestureRecognitionEngine
{
    public enum GesturePartRecognitionStatus
    {
        Failed,
        Paused,
        Succeeded
    }
}