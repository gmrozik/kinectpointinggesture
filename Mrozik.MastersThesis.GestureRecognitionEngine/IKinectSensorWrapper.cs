﻿using Microsoft.Kinect;

namespace Mrozik.MastersThesis.GestureRecognitionEngine
{
    public interface IKinectSensorWrapper
    {
        ColorImageFormat ColorImageFormat { get; }
        DepthImageFormat DepthImageFormat { get; }
        KinectSensor KinectSensor { get; }
    }
}
