﻿using System;
using Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition;

namespace Mrozik.MastersThesis.GestureRecognitionEngine
{
    public interface IGesturesEngine:IDisposable
    {
        event EventHandler<RecognizedGestureEventArgs> GestureRecognized;
        void AddGestureDefinition(IGesture gesture);
        void Start();
        void Stop();
    }
}