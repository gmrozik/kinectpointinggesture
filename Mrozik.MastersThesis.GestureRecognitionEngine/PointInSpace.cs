using System;
using System.IO;
using Microsoft.Kinect;

namespace Mrozik.MastersThesis.GestureRecognitionEngine
{
    public class PointInSpace
    {
        public SkeletonPoint CorrespondingSkeletonPoint { get; set; }
        public float ForwardInMeters { get; set; }
        public float LeftInMeters { get; set; }
        public float RightInMeters { get; set; }
        public float Height { get; set; }

        public float X
        {
            get { return RightInMeters > 0 ? RightInMeters : -LeftInMeters; }
        }

        public float Y
        {
            get { return ForwardInMeters; }
        }

        public override string ToString()
        {
            return String.Format("X: {0:N2}m Y:{1:N2}m", X, Y);
            //            return String.Format("Forward: {0:N2}m Left:{1:N2}m Right:{2:N2}m Height:{2:N2}m", ForwardInMeters, LeftInMeters, RightInMeters);
        }
    }
}