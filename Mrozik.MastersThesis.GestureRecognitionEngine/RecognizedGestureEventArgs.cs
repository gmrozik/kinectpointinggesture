﻿using System;

namespace Mrozik.MastersThesis.GestureRecognitionEngine
{
    public class RecognizedGestureEventArgs : EventArgs
    {
        public virtual string GestureName { get; set; }
    }
}