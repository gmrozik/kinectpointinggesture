﻿using Microsoft.Kinect;

namespace Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition
{
    public interface IGesturePart
    {
        GesturePartRecognitionStatus Update(Skeleton skeleton, Skeleton previousSkeleton = null);
        bool IsLongGesturePart { get; }
        double PartLengthInSeconds { get; }
    }
}