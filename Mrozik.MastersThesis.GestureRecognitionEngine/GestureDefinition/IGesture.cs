﻿using System;
using Microsoft.Kinect;

namespace Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition
{
    public interface IGesture
    {
        event EventHandler<RecognizedGestureEventArgs> GestureRecognized;
        void UpdateSkeletonData(ColorImageFrame colorImageFrame, DepthImageFrame depthImageFrame, SkeletonFrame skeletonFrame, Skeleton skeleton);
    }
}