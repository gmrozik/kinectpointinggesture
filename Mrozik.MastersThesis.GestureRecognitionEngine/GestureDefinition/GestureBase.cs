﻿using System;
using System.Collections.Generic;
using Microsoft.Kinect;

namespace Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition
{
    public abstract class GestureBase : IGesture
    {
        private Skeleton _previousSkeleton;

        private DateTime _gestureStartTime;
        protected virtual int MaxGestureLegnthInSeconds { get { return 5; } }

        private readonly IDictionary<IGesturePart, DateTime> _gesturePartStartTime = new Dictionary<IGesturePart, DateTime>();
        private readonly LinkedList<IGesturePart> _gestureParts;
        private LinkedListNode<IGesturePart> _currentGesturePart;
        private bool _paused;

        public event EventHandler<RecognizedGestureEventArgs> GestureRecognized;

        protected void OnGestureRecognized(RecognizedGestureEventArgs e)
        {
            EventHandler<RecognizedGestureEventArgs> handler = GestureRecognized;
            if (handler != null) handler(this, e);
        }

        protected GestureBase(IEnumerable<IGesturePart> gestureParts)
        {
            _gestureParts = new LinkedList<IGesturePart>(gestureParts);
            _currentGesturePart = _gestureParts.First;
        }

        public virtual void UpdateSkeletonData(ColorImageFrame colorImageFrame, DepthImageFrame depthImageFrame, SkeletonFrame skeletonFrame,
            Skeleton skeleton)
        {
            if (!IsFirstGesturePart(_currentGesturePart) && GestureTakesTooLong())
            {
                Reset();
                return;
            }

            var partUpdateResult = _currentGesturePart.Value.Update(skeleton, _previousSkeleton);

            if (partUpdateResult == GesturePartRecognitionStatus.Succeeded)
            {
                if (IsFirstGesturePart(_currentGesturePart))
                {
                    _gestureStartTime = DateTime.Now;
                }

                if (IsPartFinished(_currentGesturePart))
                {
                    if (IsLastGesturePart(_currentGesturePart))
                    {
                        NotifyGestureRecognition(colorImageFrame,depthImageFrame,skeletonFrame, skeleton);
                        Reset();
                    }
                    else
                    {
                        _currentGesturePart = _currentGesturePart.Next;
                        _paused = true;
                    }
                }
            }
            else if (partUpdateResult == GesturePartRecognitionStatus.Paused && _paused)
            {

            }
            else if (partUpdateResult == GesturePartRecognitionStatus.Failed)
            {
                Reset();
            }


            _previousSkeleton = skeleton;
        }

        private bool IsPartFinished(LinkedListNode<IGesturePart> currentGesturePartNode)
        {
            var currentGesturePart = currentGesturePartNode.Value;
            var currentDateTime = DateTime.Now;

            if (!currentGesturePart.IsLongGesturePart)
                return true;


            DateTime partStartTime;
            if (!_gesturePartStartTime.TryGetValue(currentGesturePart, out partStartTime))
            {
                partStartTime = currentDateTime;
                _gesturePartStartTime.Add(currentGesturePart, partStartTime);
            }
            return (currentDateTime - partStartTime).TotalSeconds >= currentGesturePart.PartLengthInSeconds;
        }

        private bool GestureTakesTooLong()
        {
            return (DateTime.Now - _gestureStartTime).TotalSeconds > MaxGestureLegnthInSeconds;
        }

        protected virtual void NotifyGestureRecognition(ColorImageFrame colorImageFrame, DepthImageFrame depthImageFrame, SkeletonFrame skeletonFrame, Skeleton skeleton)
        {
            OnGestureRecognized(new RecognizedGestureEventArgs());
        }

        private void Reset()
        {
            _gesturePartStartTime.Remove(_currentGesturePart.Value);
            _currentGesturePart = _gestureParts.First;
        }

        private bool IsLastGesturePart(LinkedListNode<IGesturePart> currentGesturePart)
        {
            return currentGesturePart.Next == null;
        }

        private bool IsFirstGesturePart(LinkedListNode<IGesturePart> currentGesturePart)
        {
            return currentGesturePart.Previous == null;
        }
    }
}