﻿using Microsoft.Kinect;

namespace Mrozik.MastersThesis.GestureRecognitionEngine.Utils
{
    public class SkeletonPointToSpaceConverter
    {
        public static PointInSpace ToPointInSpace(SkeletonPoint skeletonPoint)
        {
            return new PointInSpace
            {
                CorrespondingSkeletonPoint = skeletonPoint,
                ForwardInMeters = skeletonPoint.Z,
                LeftInMeters = skeletonPoint.X > 0 ? skeletonPoint.X : 0,
                RightInMeters = skeletonPoint.X < 0 ? -skeletonPoint.X : 0,
                Height = skeletonPoint.Y
            };
        }
    }
}