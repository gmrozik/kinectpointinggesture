﻿using System.Drawing;
using MathNet.Numerics.LinearAlgebra.Single;
using Microsoft.Kinect;

namespace Mrozik.MastersThesis.GestureRecognitionEngine.Utils
{
    public static class Extensions
    {
        public static DenseVector ToDenseVector(this SkeletonPoint p)
        {
            return DenseVector.OfEnumerable(new[] { p.X, p.Y, p.Z });
        }
        public static Point ToPoint(this DepthImagePoint p)
        {
            return new Point(p.X, p.Y);
        }
        public static Point ToPoint(this ColorImagePoint p)
        {
            return new Point(p.X, p.Y);
        }

        public static SkeletonPoint JointPosition(this Skeleton skeleton, JointType jointType)
        {
            return skeleton.Joints[jointType].Position;
        }
    }

}