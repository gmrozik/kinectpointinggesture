using System;
using Microsoft.Kinect;

namespace Mrozik.MastersThesis.GestureRecognitionEngine.Utils
{
    public static class TresholdVerifier
    {
        public static bool IsJointInTreshold(Skeleton skeleton1, Skeleton skeleton2, JointType jointType, Distance treshold)
        {
            return IsPositionInTreshold(skeleton1.Joints[jointType].Position, skeleton2.Joints[jointType].Position, treshold);
        }

        private static bool IsPositionInTreshold(SkeletonPoint point1, SkeletonPoint point2, Distance treshold)
        {
            return Math.Abs(point1.X - point2.X) < treshold.Metres
                   && Math.Abs(point1.Y - point2.Y) < treshold.Metres
                   && Math.Abs(point1.Z - point2.Z) < treshold.Metres;
        }
    }
}