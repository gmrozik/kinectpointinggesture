namespace Mrozik.MastersThesis.GestureRecognitionEngine
{
    public class Distance
    {
        private readonly double _centimetres;

        protected Distance(double centimetres)
        {
            _centimetres = centimetres;
        }

        public double Metres
        {
            get { return _centimetres / 100; }
        }

        public double Centimetres
        {
            get { return _centimetres; }
        }

        public static Distance FromMeters(double meters)
        {
            return new Distance(meters * 100);
        }

        public static Distance FromCentimetres(double centimetres)
        {
            return new Distance(centimetres);
        }

    }
}