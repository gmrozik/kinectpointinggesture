using System;

namespace Mrozik.MastersThesis.GestureRecognitionEngine
{
    public class Floor
    {
        public float A { get; set; }
        public float B { get; set; }
        public float C { get; set; }
        public float D { get; set; }


        public override string ToString()
        {
            return String.Format("A:{0} B:{1} C:{2} D:{3}", A, B, C, D);
        }
    }
}