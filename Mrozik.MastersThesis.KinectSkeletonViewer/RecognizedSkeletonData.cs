﻿using Microsoft.Kinect;

namespace Mrozik.MastersThesis.KinectSkeletonViewer
{
    public class RecognizedSkeletonData
    {
        public Skeleton Skeleton { get; set; }
        public FloorVector FloorVector { get; set; }
    }
}