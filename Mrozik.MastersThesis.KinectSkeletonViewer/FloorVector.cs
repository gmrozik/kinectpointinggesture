﻿using System;

namespace Mrozik.MastersThesis.KinectSkeletonViewer
{
    public class FloorVector
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public double D { get; set; }

        public static implicit operator FloorVector(Tuple<float, float, float, float> floorClipPlane)
        {
            return new FloorVector
            {
                A = floorClipPlane.Item1,
                B = floorClipPlane.Item2,
                C = floorClipPlane.Item3,
                D = floorClipPlane.Item4
            };
        }
    }
}
