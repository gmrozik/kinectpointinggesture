﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using Microsoft.Kinect;

namespace Mrozik.MastersThesis.KinectSkeletonViewer
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class KinectSkeletonViewerControl : HelixViewport3D
    {
        private static readonly Material DefaultJointSphereMaterial = new DiffuseMaterial(new SolidColorBrush(Color.FromRgb(239, 127, 26)));
        private static readonly int DefaultSkeletonBoneThickness = 3;
        private static readonly Color DefaultSkeletonBoneColor = Color.FromRgb(41, 41, 41);
        private static readonly double DefaultSkeletonScale = 3;
        private static readonly double DefaultHeadRadius = .2;
        private static readonly double DefaultHandsAndFeetRadius = .15;
        private static readonly double DefaultOtherJointsRadius = .12;


        public KinectSkeletonViewerControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty RecognizedSkeletonDataProperty = DependencyProperty.Register(
            "RecognizedSkeletonData", typeof(RecognizedSkeletonData), typeof(KinectSkeletonViewerControl), new FrameworkPropertyMetadata
            {
                PropertyChangedCallback = OnRecognizedSkeletonChanged
            });

        public RecognizedSkeletonData RecognizedSkeletonData
        {
            get { return (RecognizedSkeletonData)GetValue(RecognizedSkeletonDataProperty); }
            set { SetValue(RecognizedSkeletonDataProperty, value); }
        }

        private static void OnRecognizedSkeletonChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var skeletonData = (RecognizedSkeletonData)e.NewValue;
            var controlInstance = (KinectSkeletonViewerControl)d;
            controlInstance.OnSkeletonDetected(skeletonData);
        }


        public static readonly DependencyProperty JointSphereMaterialProperty = DependencyProperty.Register(
            "JointSphereMaterial", typeof(Material), typeof(KinectSkeletonViewerControl), new PropertyMetadata(DefaultJointSphereMaterial));
        public Material JointSphereMaterial
        {
            get { return (Material)GetValue(JointSphereMaterialProperty); }
            set { SetValue(JointSphereMaterialProperty, value); }
        }

        public static readonly DependencyProperty SkeletonBoneThicknessProperty = DependencyProperty.Register(
            "SkeletonBoneThickness", typeof(int), typeof(KinectSkeletonViewerControl), new PropertyMetadata(DefaultSkeletonBoneThickness));
        public int SkeletonBoneThickness
        {
            get { return (int)GetValue(SkeletonBoneThicknessProperty); }
            set { SetValue(SkeletonBoneThicknessProperty, value); }
        }


        public static readonly DependencyProperty SkeletonBoneColorProperty = DependencyProperty.Register(
            "SkeletonBoneColor", typeof(Color), typeof(KinectSkeletonViewerControl), new PropertyMetadata(DefaultSkeletonBoneColor));
        public Color SkeletonBoneColor
        {
            get { return (Color)GetValue(SkeletonBoneColorProperty); }
            set { SetValue(SkeletonBoneColorProperty, value); }
        }

        public static readonly DependencyProperty SkeletonScaleProperty = DependencyProperty.Register(
            "SkeletonScale", typeof(double), typeof(KinectSkeletonViewerControl), new PropertyMetadata(DefaultSkeletonScale));
        public double SkeletonScale
        {
            get { return (double)GetValue(SkeletonScaleProperty); }
            set { SetValue(SkeletonScaleProperty, value); }
        }


        public static readonly DependencyProperty HeadRadiusProperty = DependencyProperty.Register(
            "HeadRadius", typeof(double), typeof(KinectSkeletonViewerControl), new PropertyMetadata(DefaultHeadRadius));
        public double HeadRadius
        {
            get { return (double)GetValue(HeadRadiusProperty); }
            set { SetValue(HeadRadiusProperty, value); }
        }

        public static readonly DependencyProperty HandsAndFeetRadiusProperty = DependencyProperty.Register(
            "HandsAndFeetRadius", typeof(double), typeof(KinectSkeletonViewerControl), new PropertyMetadata(DefaultHandsAndFeetRadius));
        public double HandsAndFeetRadius
        {
            get { return (double)GetValue(HandsAndFeetRadiusProperty); }
            set { SetValue(HandsAndFeetRadiusProperty, value); }
        }

        public static readonly DependencyProperty OtherJointsRadiusProperty = DependencyProperty.Register(
            "OtherJointsRadius", typeof(double), typeof(KinectSkeletonViewerControl), new PropertyMetadata(DefaultOtherJointsRadius));
        public double OtherJointsRadius
        {
            get { return (double)GetValue(OtherJointsRadiusProperty); }
            set { SetValue(OtherJointsRadiusProperty, value); }
        }

        private readonly HashSet<JointType> _addedJoints = new HashSet<JointType>();

        private void OnSkeletonDetected(RecognizedSkeletonData skeletonData)
        {
            var skeleton = skeletonData.Skeleton;
            var floor = skeletonData.FloorVector;
            Placeholder3D.Children.Clear();

            _addedJoints.Clear();

            Vector3D zOffset = skeleton.Joints[JointType.HipCenter].Position.ToVector3D(-1 * SkeletonScale);
            zOffset.Z += floor.D * SkeletonScale;

            var headPosition = skeleton.Joints[JointType.Head].Position.ToPoint3D(SkeletonScale);
            headPosition += zOffset;
            AddSkeletonJoint(headPosition, HeadRadius);

            foreach (BoneOrientation bone in skeleton.BoneOrientations)
            {
                var startPosition = skeleton.Joints[bone.StartJoint].Position.ToPoint3D(SkeletonScale);
                var endPosition = skeleton.Joints[bone.EndJoint].Position.ToPoint3D(SkeletonScale);

                startPosition += zOffset;
                endPosition += zOffset;

                AddSkeletonLine(startPosition, endPosition);

                if (!_addedJoints.Contains(bone.StartJoint))
                    AddSkeletonJoint(bone.StartJoint, startPosition);
                if (!_addedJoints.Contains(bone.EndJoint))
                    AddSkeletonJoint(bone.EndJoint, endPosition);
            }
        }

        private void AddSkeletonLine(Point3D startPoint, Point3D endPoint)
        {
            var line = new LinesVisual3D
            {
                Points = new[] { startPoint, endPoint },
                Color = SkeletonBoneColor,
                Thickness = SkeletonBoneThickness
            };

            Placeholder3D.Children.Add(line);
        }
        private void AddSkeletonJoint(JointType jointType, Point3D jointPosition)
        {
            switch (jointType)
            {
                case JointType.HandLeft:
                case JointType.HandRight:
                case JointType.FootLeft:
                case JointType.FootRight:
                    AddSkeletonJoint(jointPosition, HandsAndFeetRadius);
                    break;
                default:
                    AddSkeletonJoint(jointPosition, OtherJointsRadius);
                    break;
            }
            _addedJoints.Add(jointType);
        }
        private void AddSkeletonJoint(Point3D jointPosition, double radius)
        {
            var builder = new MeshBuilder(true, true);
            var position = new Point3D(jointPosition.X, jointPosition.Y, jointPosition.Z);
            builder.AddSphere(position, radius);
            var jointSphere = new GeometryModel3D(builder.ToMesh(), JointSphereMaterial);
            var visual = new ModelVisual3D { Content = jointSphere };

            Placeholder3D.Children.Add(visual);
        }
    }


    public static class Extensions
    {
        public static Point3D ToPoint3D(this Microsoft.Kinect.SkeletonPoint skeletonPoint, double scaleFactor = 1.0)
        {
            return new Point3D(skeletonPoint.X * scaleFactor, skeletonPoint.Z * scaleFactor, skeletonPoint.Y * scaleFactor);
        }

        public static Vector3D ToVector3D(this Microsoft.Kinect.SkeletonPoint skeletonPoint, double scaleFactor = 1.0)
        {
            return new Vector3D(skeletonPoint.X * scaleFactor, skeletonPoint.Z * scaleFactor, skeletonPoint.Y * scaleFactor);
        }

        public static RotateTransform3D ToRotateTransform3D(this Microsoft.Kinect.BoneRotation boneRotation, Point3D centerOfRotation)
        {
            return new RotateTransform3D(new QuaternionRotation3D(new Quaternion(boneRotation.Quaternion.X,
                                                                                 boneRotation.Quaternion.Y,
                                                                                 boneRotation.Quaternion.Z,
                                                                                 boneRotation.Quaternion.W)), centerOfRotation);
        }
    }

}
