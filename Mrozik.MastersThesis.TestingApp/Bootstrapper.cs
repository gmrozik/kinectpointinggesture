﻿using System;
using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using KinectPointingGesture.MainWindow.ViewModels;
using KinectPointingGesture.MainWindow.Views;
using KinectPointingGesture.Utilities;
using Microsoft.Kinect.Toolkit.FaceTracking;
using Mrozik.MastersThesis.GestureRecognitionEngine;

namespace KinectPointingGesture
{

    public class Bootstrapper : Bootstrapper<MainWindowViewModel>
    {
        private readonly SimpleContainer _containerInstance = new SimpleContainer();


        protected override void Configure()
        {
            _containerInstance.Singleton<IWindowManager, WindowManager>();
            _containerInstance.Singleton<MainWindowView>();
            _containerInstance.PerRequest<IFramesRateMonitor, FramesRateMonitor>();
            _containerInstance.PerRequest<MainWindowViewModel>();
            _containerInstance.PerRequest<FaceTracker>();

            _containerInstance.PerRequest<IGesturesEngine, GesturesEngine>();
        }

        protected override object GetInstance(Type service, string key)
        {
            return _containerInstance.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _containerInstance.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _containerInstance.BuildUp(instance);
        }

    }
}
