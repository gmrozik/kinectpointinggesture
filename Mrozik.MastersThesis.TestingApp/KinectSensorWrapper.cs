using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine;

namespace KinectPointingGesture
{
    internal class KinectSensorWrapper : IKinectSensorWrapper
    {
        public ColorImageFormat ColorImageFormat { get;  set; }
        public DepthImageFormat DepthImageFormat { get;  set; }
        public KinectSensor KinectSensor { get;  set; }
    }
}