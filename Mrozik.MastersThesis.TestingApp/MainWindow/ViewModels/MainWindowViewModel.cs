﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Timers;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Caliburn.Micro;
using Coding4Fun.Kinect.Wpf;
using Kinect.Toolbox;
using KinectPointingGesture.Utilities;
using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine;
using Mrozik.MastersThesis.KinectSkeletonViewer;
using Mrozik.MastersThesis.SampleGestures;
using Mrozik.MastersThesis.SampleGestures.ComeGesture;
using Mrozik.MastersThesis.SampleGestures.PointingGesture;
using Microsoft.Kinect.Toolkit.FaceTracking;
using Mrozik.MastersThesis.SampleGestures.StopGesture;
using Rectangle = System.Drawing.Rectangle;
using Timer = System.Timers.Timer;

namespace KinectPointingGesture.MainWindow.ViewModels
{
    public class MainWindowViewModel : Screen
    {
        private static readonly ColorImageFormat ColorImageFormat = ColorImageFormat.RgbResolution640x480Fps30;
        private static readonly DepthImageFormat DepthImageFormat = DepthImageFormat.Resolution640x480Fps30;


        /// <summary>
        /// For skeleton viewer control
        /// </summary>
        public RecognizedSkeletonData RecognizedSkeletonData
        {
            get { return _recognizedSkeletonData; }
            set
            {
                if (Equals(value, _recognizedSkeletonData)) return;
                _recognizedSkeletonData = value;
                NotifyOfPropertyChange();
            }
        }

        /// <summary>
        /// Output of GestureRecognizeEngine
        /// </summary>
        public string RecognizedGestureName
        {
            get { return _recognizedGestureName; }
            set
            {
                if (value == _recognizedGestureName) return;
                _recognizedGestureName = value;
                NotifyOfPropertyChange();
            }
        }
        public string RecognizedGestureDetails
        {
            get { return _recognizedGestureDetails; }
            set
            {
                if (value == _recognizedGestureDetails) return;
                _recognizedGestureDetails = value;
                NotifyOfPropertyChange();
            }
        }


        private int _framesPerSecond;
        public int FramesPerSecond
        {
            get { return _framesPerSecond; }
            set
            {
                _framesPerSecond = value;
                NotifyOfPropertyChange();
            }
        }

        private bool _isSkeletonDetected;
        public bool IsSkeletonDetected
        {
            get { return _isSkeletonDetected; }
            set
            {
                _isSkeletonDetected = value;
                NotifyOfPropertyChange();
            }
        }

        private ColorImagePoint? _pointedPointOnImage;
        public ColorImagePoint? PointedPointOnImage
        {
            get { return _pointedPointOnImage; }
            set
            {
                if (value.Equals(_pointedPointOnImage)) return;
                _pointedPointOnImage = value;
                NotifyOfPropertyChange();
            }
        }

        private bool _isPointedPointLocated;
        public bool IsPointedPointLocated
        {
            get { return _isPointedPointLocated; }
            set
            {
                if (value.Equals(_isPointedPointLocated)) return;
                _isPointedPointLocated = value;
                NotifyOfPropertyChange();
            }
        }

        private BitmapSource _colorImage;
        public BitmapSource ColorImage
        {
            get { return _colorImage; }
            set
            {
                _colorImage = value;
                NotifyOfPropertyChange();
            }
        }

        private readonly IFramesRateMonitor _framesRateMonitor;
        private IGesturesEngine _gesturesEngine;
        private KinectSensor _kinectSensor;
        private FaceTracker _faceTracker;
        private bool _faceRecognized;
        private Rectangle _faceRectangle;

        public MainWindowViewModel(IFramesRateMonitor framesRateMonitor)
        {
            DisplayName = "Mrozik Masters Thesis Test Application";
            _framesRateMonitor = framesRateMonitor;

            _timer = new Timer();
            _timer.Elapsed += _timer_Elapsed;
            _timer.Interval = 2000;
            _timer.Enabled = false;
        }

        void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            RecognizedGestureName = "";
            RecognizedGestureDetails = "";

            _timer.Enabled = false;
        }

        private Timer _timer;
        private string _recognizedGestureName;
        private string _recognizedGestureDetails;
        private Skeleton _skeletonData;
        private RecognizedSkeletonData _recognizedSkeletonData;

        void _gesturesEngine_GestureRecognized(object sender, RecognizedGestureEventArgs e)
        {
            PointedPoint = null;
            IsPointedPointLocated = false;
            RecognizedGestureName = e.GestureName;
            RecognizedGestureDetails = "";

            if (e.GestureName == SampleGesturesNames.PoitingGesture)
            {
                var args = (PointingGestureRecognizedEventArgs)e;
                PointedPoint = args.Point;
                RecognizedGestureDetails = args.Point.ToString();
                PointedPointOnImage = SkeletonToColorPoint(args.Point);
                IsPointedPointLocated = true;
            }
            else if (e.GestureName == SampleGesturesNames.ComeGesture)
            {
                var args = (ComeGestureRecognizedEventArgs)e;
                RecognizedGestureDetails = args.Point.ToString();
            }

            if (e.GestureName == _gestUnderTests)
            {
                if (_measuring)
                {
                    _sw.Stop();
                    _measuring = false;
                    File.AppendAllLines(ResultsFileName, new[] { String.Format("{0}", _sw.ElapsedMilliseconds) });
                }
            }

            _timer.Enabled = true;
        }

        public PointInSpace PointedPoint { get; set; }

        private ColorImagePoint SkeletonToColorPoint(PointInSpace pointInSpace)
        {
            var colorPoint = _kinectSensor.CoordinateMapper.
            MapSkeletonPointToColorPoint(pointInSpace.CorrespondingSkeletonPoint, ColorImageFormat);
            return colorPoint;
        }
        public string KinectStatus
        {
            get
            {
                if (_kinectSensor == null)
                    return "Kinect device not connected";
                else
                    return _kinectSensor.Status.ToString();
            }
        }

        protected override void OnActivate()
        {
            KinectSensor.KinectSensors.StatusChanged += KinectSensors_StatusChanged;
            TryToInitializeKinect();
        }

        void KinectSensors_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            if (e.Status == Microsoft.Kinect.KinectStatus.Connected)
                TryToInitializeKinect();

            NotifyOfPropertyChange(() => KinectStatus);
        }

        private void TryToInitializeKinect()
        {
            if (_faceTracker != null)
            {
                _faceTracker.Dispose();
            }
            if (_gesturesEngine != null)
            {
                _gesturesEngine.GestureRecognized -= _gesturesEngine_GestureRecognized;
                _gesturesEngine.Stop();
                _gesturesEngine.Dispose();
            }
            var newKinectSensor = KinectSensor.KinectSensors.FirstOrDefault();
            if (newKinectSensor == null)
                return;

            var parameters = new TransformSmoothParameters
                {
                    Smoothing = 0.75f,
                    Correction = 0.1f,
                    Prediction = 0.0f,
                    JitterRadius = 0.05f,
                    MaxDeviationRadius = 0.08f
                };

            newKinectSensor.Start();
            newKinectSensor.ColorStream.Enable(ColorImageFormat);
            newKinectSensor.DepthStream.Enable(DepthImageFormat);
            newKinectSensor.SkeletonStream.Enable(parameters);
            newKinectSensor.TrySetElevationAngle(0);

            newKinectSensor.AllFramesReady += newKinectSensor_AllFramesReady;
            _kinectSensor = newKinectSensor;

            _faceTracker = new FaceTracker(_kinectSensor);
            _gesturesEngine = new GesturesEngine(new KinectSensorWrapper
            {
                KinectSensor = newKinectSensor,
                ColorImageFormat = ColorImageFormat,
                DepthImageFormat = DepthImageFormat
            },
                skeletons =>
                    skeletons.FirstOrDefault(s => s != null && s.TrackingState == SkeletonTrackingState.Tracked));

            _gesturesEngine.AddGestureDefinition(new StopGestureImpl());
            _gesturesEngine.AddGestureDefinition(new ComeGestureImpl());
            _gesturesEngine.AddGestureDefinition(new PointingGestureImpl(_faceTracker));
            _gesturesEngine.GestureRecognized += _gesturesEngine_GestureRecognized;
            _gesturesEngine.Start();

            Refresh();
        }

        void newKinectSensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            FramesPerSecond = _framesRateMonitor.GetCurrentFramesPerSecondRate();

            using (var colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame == null)
                    return;

                ColorImage = colorFrame.ToBitmapSource();


                using (var skeletonFrame = e.OpenSkeletonFrame())
                {
                    if (skeletonFrame == null)
                        return;

                    var skeletons = new Skeleton[6];
                    skeletonFrame.CopySkeletonDataTo(skeletons);

                    var skeleton =
                        skeletons.FirstOrDefault(s => s != null && s.TrackingState == SkeletonTrackingState.Tracked);

                    if (skeleton != null)
                    {
                        IsSkeletonDetected = true;
                        RecognizedSkeletonData = new RecognizedSkeletonData
                        {
                            Skeleton = skeleton,
                            FloorVector = skeletonFrame.FloorClipPlane
                        };
                    }
                }
            }
        }


        private string ResultsFileName = "Results.csv";
        private Stopwatch _sw = new Stopwatch();
        public void OnKeyDown(ActionExecutionContext context)
        {
            var args = context.EventArgs as KeyEventArgs;

            if (_gestUnderTests == SampleGesturesNames.PoitingGesture)
            {
                if (args.Key == Key.Next)
                {
                    if (PointedPoint != null)
                    {
                        File.AppendAllLines(ResultsFileName, new[] { String.Format("{0};{1}", PointedPoint.X, PointedPoint.Y) });
                    }
                }
            }

            if (_gestUnderTests == SampleGesturesNames.StopGesture || _gestUnderTests == SampleGesturesNames.ComeGesture)
            {
                if (args.Key == Key.Prior)
                {
                    _sw.Reset();
                    _sw.Start();
                    _measuring = true;
                }

            }

        }


        private string _gestUnderTests;
        private bool _measuring;
        public void StartStopGestureTesting()
        {
            _gestUnderTests = SampleGesturesNames.StopGesture;

        }

        public void StartComeGestureTesting()
        {
            _gestUnderTests = SampleGesturesNames.ComeGesture;
        }

        public void StartPointingGestureTesting()
        {
            _gestUnderTests = SampleGesturesNames.PoitingGesture;
        }
    }


}