﻿using System;

namespace KinectPointingGesture.Utilities
{
    public class FramesRateMonitor : IFramesRateMonitor
    {
        private int _totalFrames;
        private DateTime _lastFrameTime = DateTime.MaxValue;
        private int _lastFrames;
        private int _currentFrameRate = 0;
        public int GetCurrentFramesPerSecondRate()
        {
            ++_totalFrames;
            var currentTime = DateTime.Now;
            var timeDiff = currentTime.Subtract(_lastFrameTime);
            
            if (_lastFrameTime == DateTime.MaxValue || timeDiff >= TimeSpan.FromSeconds(1))
            {
                _currentFrameRate = (int)Math.Round((_totalFrames - _lastFrames) / timeDiff.TotalSeconds);
                _lastFrames = _totalFrames;
                _lastFrameTime = currentTime;
            }

            return _currentFrameRate;
        }
    }
}