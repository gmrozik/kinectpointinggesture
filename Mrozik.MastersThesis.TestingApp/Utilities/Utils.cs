﻿using System;
using System.CodeDom.Compiler;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Microsoft.Kinect;

namespace KinectPointingGesture.Utilities
{
    class Utils
    {
        public static string Point3ToString(dynamic point)
        {
            return String.Format("{0:N2} {1:N2} {2:N2}", point.X, point.Y, point.Z);
        }
        public static string Point2ToString(dynamic point)
        {
            return String.Format("{0:N2} {1:N2}", point.X, point.Y);
        }

        public static string Vector3ToString(dynamic vector)
        {
            return String.Format("{0:N2} {1:N2} {2:N2}", vector[0], vector[1], vector[2]);

        }

        public static Bitmap ImageToBitmap(byte[] colorImageBytes, int width, int height)
        {
            Bitmap bmap = new Bitmap(width, height, PixelFormat.Format32bppRgb);
            BitmapData bmapdata = bmap.LockBits(
                new Rectangle(0, 0, width, height),
                ImageLockMode.WriteOnly,
                bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            Marshal.Copy(colorImageBytes, 0, ptr, width*height*4);
            bmap.UnlockBits(bmapdata);
            return bmap;
        }
        public static Bitmap ImageToBitmap(ColorImageFrame imageFrame)
        {
            byte[] pixeldata = new byte[imageFrame.PixelDataLength];
            imageFrame.CopyPixelDataTo(pixeldata);
            Bitmap bmap = new Bitmap(imageFrame.Width, imageFrame.Height, PixelFormat.Format32bppRgb);
            BitmapData bmapdata = bmap.LockBits(
                new Rectangle(0, 0, imageFrame.Width, imageFrame.Height),
                ImageLockMode.WriteOnly,
                bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            Marshal.Copy(pixeldata, 0, ptr, imageFrame.PixelDataLength);
            bmap.UnlockBits(bmapdata);
            return bmap;
        }

    }
}
