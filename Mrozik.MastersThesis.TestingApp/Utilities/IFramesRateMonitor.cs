﻿namespace KinectPointingGesture.Utilities
{
    public interface IFramesRateMonitor
    {
        int GetCurrentFramesPerSecondRate();
    }
}