﻿using Mrozik.MastersThesis.GestureRecognitionEngine;

namespace Mrozik.MastersThesis.SampleGestures.PointingGesture
{
    public class PointingGestureRecognizedEventArgs : RecognizedGestureEventArgs
    {
        public override string GestureName
        {
            get { return SampleGesturesNames.PoitingGesture; }
        }

        public PointInSpace Point { get; set; }

        public bool WithFaceDetected { get; set; }
    }
}