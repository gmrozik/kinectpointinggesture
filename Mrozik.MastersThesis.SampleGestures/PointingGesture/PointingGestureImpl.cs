﻿using System.Collections.Generic;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit.FaceTracking;
using Mrozik.MastersThesis.GestureRecognitionEngine;
using Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition;

namespace Mrozik.MastersThesis.SampleGestures.PointingGesture
{
    public class PointingGestureImpl : GestureBase
    {
        private readonly FaceTracker _faceTracker;
        private readonly PointedPointCalculator _pointedPointCalculator;

        public PointingGestureImpl(FaceTracker faceTracker)
            : base(new List<IGesturePart>
            {
              new PointingGesturePart()  
            })
        {
            _faceTracker = faceTracker;
            _pointedPointCalculator = new PointedPointCalculator();
        }

        protected override void NotifyGestureRecognition(ColorImageFrame colorImageFrame, DepthImageFrame depthImageFrame, SkeletonFrame skeletonFrame, Skeleton skeleton)
        {
            var floor = new Floor
            {
                A = skeletonFrame.FloorClipPlane.Item1,
                B = skeletonFrame.FloorClipPlane.Item2,
                C = skeletonFrame.FloorClipPlane.Item3,
                D = skeletonFrame.FloorClipPlane.Item4
            };

            var colorImagePixels = colorImageFrame.GetRawPixelData();
            var depthImagePixels = new short[depthImageFrame.PixelDataLength];
            depthImageFrame.CopyPixelDataTo(depthImagePixels);

            var faceTrackFrame = _faceTracker.Track(colorImageFrame.Format, colorImagePixels,
                depthImageFrame.Format, depthImagePixels, skeleton);

            bool isFaceDetected;
            PointInSpace pointedPoint;
            if (faceTrackFrame != null && faceTrackFrame.TrackSuccessful)
            {
                pointedPoint = _pointedPointCalculator.CalculatePointedPoint(skeleton, floor, faceTrackFrame);
                isFaceDetected = true;
            }
            else
            {
                pointedPoint = _pointedPointCalculator.CalculatePointedPoint(skeleton, floor);
                isFaceDetected = false;
            }

            OnGestureRecognized(new PointingGestureRecognizedEventArgs
            {
                Point = pointedPoint,
                WithFaceDetected = isFaceDetected
            });
        }
    }
}
