﻿using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit.FaceTracking;
using Mrozik.MastersThesis.GestureRecognitionEngine;

namespace Mrozik.MastersThesis.SampleGestures.PointingGesture
{
    public interface IPointedPointCalculator
    {
        PointInSpace CalculatePointedPoint(Skeleton skeleton, Floor floor, FaceTrackFrame faceTrackFrame);
    }
}