﻿using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine;
using Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition;
using Mrozik.MastersThesis.GestureRecognitionEngine.Utils;

namespace Mrozik.MastersThesis.SampleGestures.PointingGesture
{
    class PointingGesturePart : IGesturePart
    {
        private static readonly Distance Treshold = Distance.FromCentimetres(5);

        public GesturePartRecognitionStatus Update(Skeleton skeleton, Skeleton previousSkeleton = null)
        {
            if (previousSkeleton != null && CompareWithPrevious(skeleton, previousSkeleton))
            {
                if (skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.ElbowRight].Position.Y)
                {
                    return GesturePartRecognitionStatus.Succeeded;
                }
            }
            return GesturePartRecognitionStatus.Failed;
        }

        private bool CompareWithPrevious(Skeleton skeleton, Skeleton previousSkeleton)
        {
            return TresholdVerifier.IsJointInTreshold(skeleton, previousSkeleton, JointType.HandRight, Treshold)
                   && TresholdVerifier.IsJointInTreshold(skeleton, previousSkeleton, JointType.ElbowRight, Treshold);

        }
        public bool IsLongGesturePart
        {
            get { return true; }
        }
        public double PartLengthInSeconds { get { return 2.0; } }
    }
}