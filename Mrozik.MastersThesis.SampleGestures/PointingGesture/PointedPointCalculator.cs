﻿using System;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit.FaceTracking;
using Mrozik.MastersThesis.GestureRecognitionEngine;
using Mrozik.MastersThesis.GestureRecognitionEngine.Utils;

namespace Mrozik.MastersThesis.SampleGestures.PointingGesture
{
    public class PointedPointCalculator : IPointedPointCalculator
    {
        public PointInSpace CalculatePointedPoint(Skeleton skeleton, Floor floor, FaceTrackFrame faceTrackFrame = null)
        {
            var vectorStartingPoint = ResolveStartingPoint(skeleton, faceTrackFrame);
            var vectorEndingPoint = skeleton.Joints[JointType.HandRight].Position;

            var directionVector = vectorEndingPoint.ToDenseVector().Subtract(vectorStartingPoint.ToDenseVector());


            SkeletonPoint pointOnTheFloor;

            if (Math.Abs(floor.A * directionVector[0] + floor.B * directionVector[1] + floor.C * directionVector[2]) > 0.01)
            {
                var t =
                    -(floor.A * vectorEndingPoint.X + floor.B * vectorEndingPoint.Y + floor.C * vectorEndingPoint.Z + floor.D) /
                    (floor.A * directionVector[0] + floor.B * directionVector[1] + floor.C * directionVector[2]);
                pointOnTheFloor = new SkeletonPoint
                {
                    X = t * directionVector[0] + vectorEndingPoint.X,
                    Y = t * directionVector[1] + vectorEndingPoint.Y,
                    Z = t * directionVector[2] + vectorEndingPoint.Z
                };
            }
            else
            {
                pointOnTheFloor = new SkeletonPoint
                {
                    X = vectorEndingPoint.X,
                    Y = vectorEndingPoint.Y,
                    Z = vectorEndingPoint.Z
                };
            }

            return SkeletonPointToSpaceConverter.ToPointInSpace(pointOnTheFloor);
        }

        protected virtual SkeletonPoint ResolveStartingPoint(Skeleton skeleton, FaceTrackFrame faceTrackFrame)
        {
            return skeleton.Joints[JointType.ElbowRight].Position;
            //if (faceTrackFrame == null)
            //    return skeleton.Joints[JointType.Head].Position;
            //else
            //{
            //    var leftEyePosition = faceTrackFrame.Get3DShape()[FeaturePoint.InnerCornerLeftEye];
            //    var rightEyePosition = faceTrackFrame.Get3DShape()[FeaturePoint.InnerCornerRightEye];
            //    return new SkeletonPoint
            //    {
            //        X = (leftEyePosition.X + rightEyePosition.X) / 2,
            //        Y = leftEyePosition.Y,
            //        Z = leftEyePosition.Z
            //    };
            //}
        }
    }

}