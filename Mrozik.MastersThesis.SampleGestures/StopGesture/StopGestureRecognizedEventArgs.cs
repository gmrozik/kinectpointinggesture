﻿using Mrozik.MastersThesis.GestureRecognitionEngine;

namespace Mrozik.MastersThesis.SampleGestures.StopGesture
{
    public class StopGestureRecognizedEventArgs : RecognizedGestureEventArgs
    {
        public override string GestureName
        {
            get { return SampleGesturesNames.StopGesture; }
        }

    }
}