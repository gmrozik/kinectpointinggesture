﻿using System.Collections.Generic;
using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition;

namespace Mrozik.MastersThesis.SampleGestures.StopGesture
{
    public class StopGestureImpl : GestureBase
    {
        public StopGestureImpl()
            : base(new List<IGesturePart>
            {
                new BetterStopGesturePart()
            })
        {

        }

        protected override void NotifyGestureRecognition(ColorImageFrame colorImageFrame, DepthImageFrame depthImageFrame, SkeletonFrame skeletonFrame, Skeleton skeleton)
        {
            OnGestureRecognized(new StopGestureRecognizedEventArgs());
        }
    }
}
