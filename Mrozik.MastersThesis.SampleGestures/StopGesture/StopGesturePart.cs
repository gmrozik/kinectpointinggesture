using System;
using System.IO;
using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine;
using Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition;
using Mrozik.MastersThesis.GestureRecognitionEngine.Utils;

namespace Mrozik.MastersThesis.SampleGestures.StopGesture
{
    public class BetterStopGesturePart : StopGesturePart
    {
        public override GesturePartRecognitionStatus Update(Skeleton skeleton, Skeleton previousSkeleton = null)
        {
            if (previousSkeleton != null && CompareWithPrevious(skeleton, previousSkeleton))
            {
                var handPosition = skeleton.JointPosition(JointType.HandRight);
                var elbowPosition = skeleton.JointPosition(JointType.ElbowRight);

                if (IsOnLine(handPosition,skeleton.Position) && IsOnLine(elbowPosition,skeleton.Position))
                {
                    if (Math.Abs(skeleton.JointPosition(JointType.HandRight).Y - skeleton.JointPosition(JointType.ElbowRight).Y) < YArmTreshold.Metres)
                    {
                        return GesturePartRecognitionStatus.Succeeded;
                    }
                }
            }
            return GesturePartRecognitionStatus.Failed;
        }

        private bool IsOnLine(SkeletonPoint jointPoint, SkeletonPoint operatorPosition)
        {
            if (Math.Abs(operatorPosition.X) < XArmTreshold.Metres)
            {
                return Math.Abs(jointPoint.X) < XArmTreshold.Metres;
            }
            else
            {
                var a = operatorPosition.Z / operatorPosition.X;
                var result = jointPoint.Z - (a * jointPoint.X);
                return Math.Abs(result) < XArmTreshold.Metres;
            }
        }
    }
    public class StopGesturePart : IGesturePart
    {
        protected static readonly Distance XArmTreshold = Distance.FromCentimetres(40);
        protected static readonly Distance YArmTreshold = Distance.FromCentimetres(20);

        private static readonly Distance Treshold = Distance.FromCentimetres(1);


        public virtual GesturePartRecognitionStatus Update(Skeleton skeleton, Skeleton previousSkeleton = null)
        {
            if (previousSkeleton != null && CompareWithPrevious(skeleton, previousSkeleton))
            {
                if (skeleton.JointPosition(JointType.HandRight).Y >= skeleton.JointPosition(JointType.ElbowRight).Y)
                {
                    if (Math.Abs(skeleton.JointPosition(JointType.HandRight).X - skeleton.JointPosition(JointType.ElbowRight).X) < XArmTreshold.Metres)
                    {
                        if (skeleton.JointPosition(JointType.HandRight).Z < skeleton.JointPosition(JointType.ElbowRight).Z)
                        {
                            return GesturePartRecognitionStatus.Succeeded;
                        }
                    }
                }
            }
            return GesturePartRecognitionStatus.Failed;
        }

        protected bool CompareWithPrevious(Skeleton skeleton, Skeleton previousSkeleton)
        {
            return TresholdVerifier.IsJointInTreshold(skeleton, previousSkeleton, JointType.HandRight, Treshold)
                   && TresholdVerifier.IsJointInTreshold(skeleton, previousSkeleton, JointType.ElbowRight, Treshold);

        }

        public bool IsLongGesturePart
        {
            get { return true; }
        }
        public double PartLengthInSeconds
        {
            get { return 0.1; }
        }
    }
}