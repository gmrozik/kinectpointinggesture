﻿namespace Mrozik.MastersThesis.SampleGestures
{
    public static class SampleGesturesNames
    {
        public static string PoitingGesture = "PointingGesture";
        public static string StopGesture = "StopGesture";
        public static string ComeGesture = "ComeGesture";
    }
}