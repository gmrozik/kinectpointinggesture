﻿using System.Collections.Generic;
using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition;

namespace Mrozik.MastersThesis.SampleGestures.ComeGesture
{
    public class ComeGestureImpl : GestureBase
    {
        private readonly ComePointCalculator _comePointCalculator = new ComePointCalculator();

        public ComeGestureImpl()
            : base(new List<IGesturePart>
            {
                new ComeGesturePart1(),
                new ComeGesturePart2(),
                new ComeGesturePart1(),
                new ComeGesturePart2()
            })
        {

        }

        protected override void NotifyGestureRecognition(ColorImageFrame colorImageFrame, DepthImageFrame depthImageFrame, SkeletonFrame skeletonFrame, Skeleton skeleton)
        {
            var comePoint = _comePointCalculator.CalculateComePoint(skeleton);
            OnGestureRecognized(new ComeGestureRecognizedEventArgs
            {
                Point = comePoint
            });
        }
    }
}
