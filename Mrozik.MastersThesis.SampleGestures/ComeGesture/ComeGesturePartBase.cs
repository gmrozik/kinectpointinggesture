using System;
using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine;
using Mrozik.MastersThesis.GestureRecognitionEngine.GestureDefinition;

namespace Mrozik.MastersThesis.SampleGestures.ComeGesture
{
    abstract class ComeGesturePartBase : IGesturePart
    {
        protected static readonly Distance XArmTreshold = Distance.FromCentimetres(40);

        protected bool IsOnLine(SkeletonPoint jointPoint, SkeletonPoint operatorPosition)
        {
            if (Math.Abs(operatorPosition.X) < XArmTreshold.Metres)
            {
                return Math.Abs(jointPoint.X) < XArmTreshold.Metres;
            }
            else
            {
                var a = operatorPosition.Z / operatorPosition.X;
                var result = jointPoint.Z - (a * jointPoint.X);
                return Math.Abs(result) < XArmTreshold.Metres;
            }
        }

        public abstract GesturePartRecognitionStatus Update(Skeleton skeleton, Skeleton previousSkeleton = null);

        public bool IsLongGesturePart
        {
            get { return false; }
        }
        public double PartLengthInSeconds { get; private set; }
    }
}