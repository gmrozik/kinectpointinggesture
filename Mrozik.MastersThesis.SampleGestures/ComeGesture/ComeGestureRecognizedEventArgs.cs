﻿using Mrozik.MastersThesis.GestureRecognitionEngine;

namespace Mrozik.MastersThesis.SampleGestures.ComeGesture
{
    public class ComeGestureRecognizedEventArgs : RecognizedGestureEventArgs
    {
        public override string GestureName
        {
            get { return SampleGesturesNames.ComeGesture; }
        }

        public PointInSpace Point { get; set; }
    }
}