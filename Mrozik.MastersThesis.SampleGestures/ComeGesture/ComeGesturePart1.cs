using System;
using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine;
using Mrozik.MastersThesis.GestureRecognitionEngine.Utils;

namespace Mrozik.MastersThesis.SampleGestures.ComeGesture
{
    class ComeGesturePart1 : ComeGesturePartBase
    {
        public override GesturePartRecognitionStatus Update(Skeleton skeleton, Skeleton previousSkeleton = null)
        {
            if (skeleton.JointPosition(JointType.HandRight).Y > skeleton.JointPosition(JointType.ElbowRight).Y)
            {
                if (IsOnLine(skeleton.JointPosition(JointType.HandRight), skeleton.Position))
                {
                    if (skeleton.JointPosition(JointType.HandRight).Z < skeleton.JointPosition(JointType.ElbowRight).Z)
                    {
                        return GesturePartRecognitionStatus.Succeeded;
                    }
                    return GesturePartRecognitionStatus.Paused;
                }
            }

            return GesturePartRecognitionStatus.Failed;
        }

    }

}