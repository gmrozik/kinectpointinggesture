using Microsoft.Kinect;
using Mrozik.MastersThesis.GestureRecognitionEngine;
using Mrozik.MastersThesis.GestureRecognitionEngine.Utils;

namespace Mrozik.MastersThesis.SampleGestures.ComeGesture
{
    internal class ComePointCalculator
    {
        public PointInSpace CalculateComePoint(Skeleton skeleton)
        {
            var point = skeleton.Joints[JointType.Head].Position;
            return SkeletonPointToSpaceConverter.ToPointInSpace(point);
        }
    }
}